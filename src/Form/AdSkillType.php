<?php

namespace App\Form;

use App\Entity\Ad;
use App\Entity\Competence;
use App\Entity\AdSkill;

use App\Form\AdType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class AdSkillType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('level', ChoiceType::class, [
                'choices' => [
                    'débutant' => 'débutant',
                    'intermédiaire' => 'intermédiaire',
                    'expérimenté' => 'expérimenté'
                ],
                'expanded' => true
            ])
            ->add('competence', EntityType::class, [
                'by_reference' => false,
                'class' => Competence::class,
                'choice_label' => 'name',
                'required' => true,
                'multiple' => true
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => AdSkill::class
        ]);
    }
}

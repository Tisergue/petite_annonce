<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CompetenceRepository")
 */
class Competence
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\AdSkill", mappedBy="competence", cascade={"persist", "remove"})
     */
    private $adSkills;

    public function __construct()
    {
        $this->adSkills = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|AdSkill[]
     */
    public function getAdSkills(): Collection
    {
        return $this->adSkills;
    }

    public function addAdSkill(AdSkill $adSkill): self
    {
        if (!$this->adSkills->contains($adSkill)) {
            $this->adSkills[] = $adSkill;
            $adSkill->addCompetence($this);
        }

        return $this;
    }

    public function removeAdSkill(AdSkill $adSkill): self
    {
        if ($this->adSkills->contains($adSkill)) {
            $this->adSkills->removeElement($adSkill);
            $adSkill->removeCompetence($this);
        }

        return $this;
    }
}

<?php

namespace App\Controller;

use App\Entity\User;

use App\Form\UserEditType;

use Doctrine\Common\Persistence\ObjectManager;

use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AccountController extends AbstractController
{
    /**
     * @Route("/account", name="account")
     */
    public function index()
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $repo           = $this->getDoctrine()->getRepository(User::class);
        $userConnected  = $this->getUser();
        $user           = $repo->findOneById($userConnected->getId());

        return $this->render('account/index.html.twig', [
            'user' => $user
        ]);
    }

    /**
     * @Route("/account/edit", name="account_edit")
     */
    public function edit(Request $req, UserPasswordEncoderInterface $encoder, ObjectManager $manager) {

        // https://roadtodev.com/fr/blog/symfony-4-reset-password

        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $userConnected  = $this->getUser();

        if ($userConnected) {
            $user = $this->getDoctrine()->getRepository(User::class)
                         ->findOneById($userConnected->getId());

            $form = $this->createForm(UserEditType::class, $user);

            $form->handleRequest($req);

            if ($form->isSubmitted() && $form->isValid()) {
                $oldPassword = $req->request->get('check');

                if ($encoder->isPasswordValid($user, $oldPassword)) {
                    $newEncodedPassword = $passwordEncoder->encodePassword($user, $user->getPlainPassword());
                    $user->setPassword($newEncodedPassword);
                    
                    $manager->persist($user);
                    $manager->flush();
    
                    $this->addFlash('notice', 'Votre mot de passe à bien été changé !');
    
                    return $this->redirectToRoute('account');
                } else {
                    $form->addError(new FormError('Ancien mot de passe incorrect'));
                }
            }
            
            return $this->render('account/edit.html.twig', [
                'form' => $form->createView()
            ]);
        }

        $this->addFlash('error', 'Vous devez être connecté pour accéder à cette section.');

        return $this->redirectToRoute('main');
    }
}

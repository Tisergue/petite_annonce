<?php

namespace App\Controller;

use App\Entity\Ad;
use App\Entity\AdPost;
use App\Entity\AdSkill;

// use App\Service\Title;

use App\Form\AdType;
use App\Form\AdSkillType;
use App\Form\AdPostType;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
// use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Persistence\ObjectManager;

class AdController extends Controller {
    /**
     * @Route("/annonces", name="ad")
     */
    public function index(Request $req) {
        $repo       = $this->getDoctrine()->getRepository(Ad::class);

        $ads        = $repo->findAllSort();
        $lastAds    = $this->getLastAds($repo);

        $totalAds   = count($ads);

        $paginator  = $this->get('knp_paginator');
        $adsPagined = $paginator->paginate($ads, $req->query->getInt('page', 1), 4);

        return $this->render('ad/index.html.twig', [
            'ads' => $adsPagined,
            'lastAd' => $lastAds,
            'totalAds' => $totalAds,
            'page' => $req->query->getInt('page', 1)
        ]);
    }

    /**
     * @Route("/mes_candidatures", name="my_candidacy")
     */
    public function myCandidacy(Request $req) {
        $repo = $this->getDoctrine()->getRepository(Ad::class);

        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $user = $this->getUser();

        $ads = $repo->findByCandidacy($user->getUsername());

        $paginator  = $this->get('knp_paginator');
        $adsPagined = $paginator->paginate($ads, $req->query->getInt('page', 1), 4);

        return $this->render('ad/myCandidacy.html.twig', [
            'ads' => $adsPagined
        ]);
    }

    /**
     * @Route("/mes_annonces/{id}", name="my_ad")
     */
    public function myAds($id, Request $req) {
        $repo = $this->getDoctrine()->getRepository(Ad::class);
        $ads = $repo->findByUser($id);

        $paginator  = $this->get('knp_paginator');
        $adsPagined = $paginator->paginate($ads, $req->query->getInt('page', 1), 4);

        return $this->render('ad/myAds.html.twig', [
            'ads' => $adsPagined
        ]);  
    }

    /**
     * @Route("/edit_candidacy/{id}", name="edit_candidacy")
     * @Route("/rm_candidacy/{id}", name="rm_candidacy")
     */
    public function editCandidacy($id, Request $req, ObjectManager $manager, AdPost $adPost = null) {
        if (!$adPost) {
            echo 'whooooops';
            die();
        }
        $actualRoute = $req->attributes->get('_route');

        $repo = $this->getDoctrine()->getRepository(AdPost::class);
        $posts = $repo->findOneById($id);

        $form = $this->createForm(AdPostType::class, $adPost);
        $form->handleRequest($req);

        $redirect = $adPost->getAd()->getId();

        if ($actualRoute === 'rm_candidacy') {
            $manager->remove($adPost);
            $manager->flush();

            $this->addFlash('success', 'Votre candidature a bien été supprimé !');
            
            return $this->redirectToRoute('ad_detail', ['id' => $redirect]);
        } else if ($form->isSubmitted() && $form->isValid()) {
            $manager->persist($adPost);
            $manager->flush();

            $this->addFlash('success', 'Votre candidature a bien été modifié !');

            return $this->redirectToRoute('ad_detail', ['id' => $redirect]);
        }

        return $this->render('ad/edit_candidacy.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/annonces/{id}", name="ad_detail")
     */
    public function detail($id, Request $req, ObjectManager $manager) {
        $repo = $this->getDoctrine()->getRepository(Ad::class);
        $ad = $repo->findOneById($id);

        $adPost = new AdPost;
        $repo = $this->getDoctrine()->getRepository(AdPost::class);
        $posts = $repo->findBy(['ad' => $id]);


        $form = $this->createForm(AdPostType::class, $adPost);
        $form->handleRequest($req);

        if ($form->isSubmitted() && $form->isValid()) {
            $adPost->setCreatedAt(new \Datetime);

            $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
            $user = $this->getUser();

            $adPost->setUsername($user->getUsername());
            $adPost->setAd($ad);

            $manager->persist($adPost);
            $manager->flush();

            $this->addFlash('success', 'Votre candidature a bien été enregistré !');

            return $this->redirectToRoute('ad_detail', ['id' => $id]);
        }

        return $this->render('ad/detail.html.twig', [
            'ad' => $ad,
            'posts' => $posts,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/new_ad", name="new_ad")
     * @Route("/edit_ad/{id}", name="edit_ad")
     */
    public function manageAd(Request $req, ObjectManager $manager, Ad $ad = null) {
        $edit = false;
        
        if (!$ad) {
            $ad = new Ad;
        } else if ($ad) {
            $collection = new ArrayCollection;

            foreach ($ad->getSkill() as $skill) {
                $collection->add($skill);
            }

            $edit = true;
        }

        $form = $this->createForm(AdType::class, $ad);
        
        // var_dump($form->get('skill')); die();

        $form->handleRequest($req);

        if($form->isSubmitted() && $form->isValid()) {
            if (!$ad->getId()) {
                $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
                $user = $this->getUser();

                $ad->setCreatedAt(new \DateTime);
                $ad->setUser($user);
            }

            $content = $ad->getContent();
            $content = nl2br($content);

            $ad->setContent($content);

            $manager->persist($ad);
            $manager->flush();

            return $this->redirectToRoute('ad_detail', [
                'id' => $ad->getId()
            ]);
        }

        return $this->render('ad/form.html.twig', [
            'form' => $form->createView(),
            'edit' => $edit
        ]);
    }

    /**
     * @Route("/rm_ad/{id}", name="rm_ad")
     */
    public function del($id, ObjectManager $manager) {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $userID = $this->getUser()->getId();

        $repo = $this->getDoctrine()->getRepository(Ad::class);
        $rm = $repo->findOneById($id);

        if ($rm->getUser()->getId() === $userID) {
            $manager->remove($rm);
            $manager->flush();
        }

        return $this->redirectToRoute('main');
    }

    public function getLastAds($repo) {
        $lastAd = $repo->findLastAds();

        return $lastAd;
    }
}

<?php

namespace App\Repository;

use App\Entity\Ad;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Ad|null find($id, $lockMode = null, $lockVersion = null)
 * @method Ad|null findOneBy(array $criteria, array $orderBy = null)
 * @method Ad[]    findAll()
 * @method Ad[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AdRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Ad::class);
    }

    public function findAllSort($sort = 'DESC') {
        return $this->createQueryBuilder('a')
            ->orderBy('a.createdAt', $sort)
            ->getQuery()
            ->getResult();
    }

    public function findByCandidacy($user) {
        return $this->createQueryBuilder('i')
            ->innerJoin('i.adPosts', 'p')
            ->where('p.username = :val')
            ->setParameter('val', $user)
            ->getQuery()
            ->getResult();
    }

    public function findByUser($val) {
        // SELECT * FROM `ad` WHERE user_id = 10
        return $this->createQueryBuilder('a')
            ->where('a.user = :val')
            ->setParameter('val', $val)
            ->getQuery()
            ->getResult();
    }

    public function findLastAds() {
        // SELECT * FROM `ad` ORDER BY coalesce(update_at, created_at) DESC LIMIT 5
        
        return $this->createQueryBuilder('a')
            ->addOrderBy('a.updateAt', 'DESC')
            ->addOrderBy('a.createdAt', 'DESC')
            ->setMaxResults(5)
            ->getQuery()
            ->getResult();

        // $conn = $this->getEntityManager()->getConnection();

        // $sql = '
        //     SELECT ad.*, u.username
        //     FROM ad ad
        //     INNER JOIN user u
        //     ORDER BY coalesce(ad.update_at, ad.created_at) DESC LIMIT 5';
        // $stmt = $conn->prepare($sql);
        // $stmt->execute();

        // return $stmt->fetchAll();
    }

    // /**
    //  * @return Ad[] Returns an array of Ad objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Ad
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

<?php

namespace App\Repository;

use App\Entity\AdPost;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method AdPost|null find($id, $lockMode = null, $lockVersion = null)
 * @method AdPost|null findOneBy(array $criteria, array $orderBy = null)
 * @method AdPost[]    findAll()
 * @method AdPost[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AdPostRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, AdPost::class);
    }

    // /**
    //  * @return AdPost[] Returns an array of AdPost objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AdPost
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

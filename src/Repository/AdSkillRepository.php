<?php

namespace App\Repository;

use App\Entity\AdSkill;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method AdSkill|null find($id, $lockMode = null, $lockVersion = null)
 * @method AdSkill|null findOneBy(array $criteria, array $orderBy = null)
 * @method AdSkill[]    findAll()
 * @method AdSkill[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AdSkillRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, AdSkill::class);
    }

    // /**
    //  * @return AdSkill[] Returns an array of AdSkill objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AdSkill
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

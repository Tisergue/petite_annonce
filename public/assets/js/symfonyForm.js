function rm(id) {
    var toRm = document.getElementById(id)

    toRm.remove()
}

function addSkillFormDeleteLink($skillFormLi) {
    var $removeFormButton = $('<button type="button" class="btn btn-danger">Supprimer</button>')
    $skillFormLi[1].append($removeFormButton)

    $removeFormButton.on('click', function(e) {
        $skillFormLi.map(elem => {
            elem.remove()
        })
    })
}

function addSkillForm($collectionHolder, $newLinkLi) {
    addSkillFormDeleteLink(
        $collectionHolder.map(elem => {
            var prototype   = elem.data('prototype')
            var index       = elem.data('index')
            var newForm     = prototype

            newForm = newForm.replace(/__name__/g, index)
            elem.data('index', index + 1)

            var $newFormLi = $('<li class="mb-3" style="list-style-type:none"></li>').append(newForm)

            $newLinkLi.before($newFormLi)

            return $newFormLi
        })
    )
}

var $collectionHolder
var $addCompetenceButton = $('<button type="button" class="add_Competence_link btn btn-primary">Ajouter une compétence</button>')
var $newLinkLi = $('<li style="list-style-type:none"></li>').append($addCompetenceButton)

jQuery(document).ready(function() {
    $collectionHolder1 = $('ul.competence')
    $collectionHolder1.append($newLinkLi)
    $collectionHolder1.data('index', $collectionHolder1.find(':input').length)

    $collectionHolder2 = $('ul.level')
    $collectionHolder2.append($newLinkLi)
    $collectionHolder2.data('index', $collectionHolder2.find(':input').length)

    tab = [
        $collectionHolder1, $collectionHolder2
    ]

    $addCompetenceButton.on('click', function(e) {
        addSkillForm(tab, $newLinkLi)
    })
})
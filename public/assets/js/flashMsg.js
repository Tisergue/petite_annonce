var msg = document.querySelector('.flash-msg')

if (msg) {
    msg.addEventListener('click', () => {
        msg.remove()
    })
}